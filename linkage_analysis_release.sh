### Script to generate a table of 500 samplings of 5/3 ends of reads mapped to a TE.

#-------------------------------------------------------------------------------------------------------
#presetting of variables 
NAMEforANALYSIS=

#Folder of files to keep - use for other analysis
rawFOLDER=

#output folder
rawOUTPUT=

#TE name
TE=

#length of TE
LENGTH=

#-------------------------------------------------------------------------------------------------------
#prepare all vairables
OUTPUT=${rawOUTPUT}/${NAMEforANALYSIS}

#input bed file
INPUT=${rawFOLDER}/${NAMEforANALYSIS}/${NAMEforANALYSIS}_TE_3MM_collapsed.bed
#-------------------------------------------------------------------------------------------------------

#create all folders
mkdir -p ${rawOUTPUT}
mkdir -p ${rawOUTPUT}/TE_coordinates
mkdir -p ${rawOUTPUT}/${NAMEforANALYSIS}
mkdir -p ${rawOUTPUT}/${NAMEforANALYSIS}/ends
mkdir -p ${rawOUTPUT}/${NAMEforANALYSIS}/ends/${TE}
mkdir -p ${rawOUTPUT}/${NAMEforANALYSIS}/tables
mkdir -p ${rawOUTPUT}/${NAMEforANALYSIS}/tables/${TE}

echo "start"
echo ${NAMEforANALYSIS}

### create coordinate file
if [ ! -f ${rawOUTPUT}/TE_coordinates/${TE}_coordinate.txt ]; then
for ((i=0; i<=${LENGTH}-1; i++)); do
printf $i"\n" >> ${rawOUTPUT}/TE_coordinates/${TE}_coordinate.txt
done
fi

#### INPUT file looks as follows:
#<TE> <start> <end> <sequenceID @ read counts> <> <strand>
#412     5623    5651    HWI-ST1253F-0259:8:1101:10000:11460#26166@12    255     -
#mdg1    1364    1387    HWI-ST1253F-0259:8:1101:10000:24781#26166@13    255     -
#Circe   5607    5632    HWI-ST1253F-0259:8:1101:10000:42938#26166@3     255     +
#Cr1a    285     309     HWI-ST1253F-0259:8:1101:10000:61324#26166@3     255     +

###### reporting 5' and 3' end coordinates
awk -v TE=${TE} '($1==TE&&$6=="+") {split($4,a,"@"); for (i=1;i<=a[2];i++) print $2}' ${INPUT} > ${rawOUTPUT}/${NAMEforANALYSIS}/ends/${TE}/${NAMEforANALYSIS}_${TE}_plus_5end.txt
awk -v TE=${TE} '($1==TE&&$6=="-") {split($4,a,"@"); for (i=1;i<=a[2];i++) print $3-1}' ${INPUT} > ${rawOUTPUT}/${NAMEforANALYSIS}/ends/${TE}/${NAMEforANALYSIS}_${TE}_minus_5end.txt
awk -v TE=${TE} '($1==TE&&$6=="+") {split($4,a,"@"); for (i=1;i<=a[2];i++) print $3-1}' ${INPUT} > ${rawOUTPUT}/${NAMEforANALYSIS}/ends/${TE}/${NAMEforANALYSIS}_${TE}_plus_3end.txt
awk -v TE=${TE} '($1==TE&&$6=="-") {split($4,a,"@"); for (i=1;i<=a[2];i++) print $2}' ${INPUT} > ${rawOUTPUT}/${NAMEforANALYSIS}/ends/${TE}/${NAMEforANALYSIS}_${TE}_minus_3end.txt
	

##### 500 downsampling --- BEGIN ---
### sample 20% of the TE length
SAMPLING=$(echo "${LENGTH}/5" | bc)
	
### Randomly take the coordinates from the bed file for ${SAMPLING} times from plus and minus strands. "shuf" allows random sampling.
for m in plus minus; do

#### replicates of 500 downsampling
for ((j=1; j<=500; j++)); do

### this is to create a table column of reporting the counts of every coordinate. To report 0 counts, I first add a pseudocount of 1 to every coordinate before sorting and uniq, which then is substracted at the end.
for k in 5end 3end; do

shuf ${rawOUTPUT}/${NAMEforANALYSIS}/ends/${TE}/${NAMEforANALYSIS}_${TE}_${m}_${k}.txt -n ${SAMPLING} > ${rawOUTPUT}/${NAMEforANALYSIS}/tables/${TE}/${NAMEforANALYSIS}_${TE}_${m}_${k}_${j}.txt
### add a pseudo count of one to every coordinate and substract it later.
cat ${rawOUTPUT}/TE_coordinates/${TE}_coordinate.txt ${rawOUTPUT}/${NAMEforANALYSIS}/tables/${TE}/${NAMEforANALYSIS}_${TE}_${m}_${k}_${j}.txt | sort --parallel=3 -k1,1n | uniq -c | awk '{print $1-1}' > ${rawOUTPUT}/${NAMEforANALYSIS}/tables/${TE}/${NAMEforANALYSIS}_${TE}_${m}_${k}_table_${j}.txt

done

done

### simply paste them in a single file and also to print the counts at every position of TE
for k in 5end 3end; do
paste ${rawOUTPUT}/${NAMEforANALYSIS}/tables/${TE}/${NAMEforANALYSIS}_${TE}_${m}_${k}_table_*.txt > ${rawOUTPUT}/${NAMEforANALYSIS}/tables/${TE}/${NAMEforANALYSIS}_${TE}_${m}_${k}_500tables.txt
cat ${rawOUTPUT}/TE_coordinates/${TE}_coordinate.txt ${rawOUTPUT}/${NAMEforANALYSIS}/ends/${TE}/${NAMEforANALYSIS}_${TE}_${m}_${k}.txt |\
sort --parallel=3 -k1,1n | uniq -c | awk '{print $1-1}' > ${rawOUTPUT}/${NAMEforANALYSIS}/ends/${TE}/${NAMEforANALYSIS}_${TE}_${m}_${k}_counts.txt
done

done
##### 500 downsampling --- END ---
